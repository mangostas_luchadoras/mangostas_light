package light_out_negocio;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class CasillaTest 
{
	@Test
	public void cambiarLuzPrendidaTest()
	{
		int n=0;
		Casilla uno = new Casilla(true);
		n+=uno.cambiarLuz();
		assertEquals(1,n);
		assertFalse(uno.getLuz());
	}
	
	@Test
	public void cambiarLuzApagadaTest()
	{
		int n=0;
		Casilla uno = new Casilla(false);
		n+=uno.cambiarLuz();
		assertEquals(-1,n);
		assertTrue(uno.getLuz());
	}

}
