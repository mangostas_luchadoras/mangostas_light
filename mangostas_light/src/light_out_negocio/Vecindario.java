package light_out_negocio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Vecindario 
{
	protected ArrayList<Set<Integer>> vecinos;
	protected int tamanio;
	
	/**
	 * Construye un vecindario (usando un ArrayList de Sets de enteros). Simulando una matriz de tamaño n, cada Set se incicializa
	 * con un conjuto de los valores que tiene a cada lado, arriba y abajo 
	 * (en caso de no tener alguno son omitidos).
	 * @param n tamaño del vecindario
	 */
	public Vecindario(int n) 
	{
		tamanio = n;
		vecinos = new ArrayList<Set<Integer>>();
		llenarVecinos();
	}
	
	/**
	 * Crea los Sets del vecindario y los inicializa con sus valores correspondientes.
	 */
	protected void llenarVecinos()
	{
		for(int i = 0; i < tamanio*tamanio; i++)
			vecinos.add(new HashSet<Integer>());
		for(int i = 0; i < vecinos.size(); i++)
			vecinos.get(i).addAll(conjuntoDeVecinos(i));
	}
	
	/**
	 * Busca los vecinos de un entero.
	 * @param n entero
	 * @return Retorna Set con el indice de sus vecinos
	 */
	public Set<Integer> getVecinos(int n)
	{
		return vecinos.get(n);
	}
	
	/**
	 * Busca el conjunto de vecinos de acuerdo a la posición n.
	 * @param n entero
	 * @return Set de enteros con los vecinos de n (sin incluir)
	 */
	protected Set<Integer> conjuntoDeVecinos (int n) 
	{
		Set<Integer> ret = new HashSet<Integer>();
		switch(posicion(n))
		{
			case 0: return vecinosEsquinas(n);
			case 1: return vecinosPrimeraFila(n);
			case 2:	return vecinosUltimaFila(n);
			case 3:	return vecinosPrimeraColumna(n);
			case 4: return vecinosUltimaColumna(n);
			case 5:	return vecinosMedio(n);
		}
		return ret;
	}
	
	/**
	 * Determina la posición de un entero n en el vecindario.
	 * @param n entero
	 * @return Retorna 0 si es una esquina, 1 si esta en la primer fila, 2 si esta en la ultima fila,
	 * 3 si esta en la primer columna, 4 si esta en la ultima columna y 5 si esta en el medio
	 */
	private int posicion(int n)
	{
		if (n == 0 || n == tamanio -1 || n == vecinos.size() -tamanio || n == vecinos.size() -1)
			return 0;
		if (n > 0 && n < tamanio -1)
			return 1;
		if (n > vecinos.size()- tamanio && n < vecinos.size() -1) 
			return 2;
		if (n%tamanio == 0) 
			return 3;
		if (n%tamanio == tamanio -1) 
			return 4;
		else
			return 5;
	}
	
	/**
	 * Comprueba que esquina representa el entero n para devolver un Set con sus vecinos.
	 * @param n entero
	 * @return Retorna Set con los vecinos de la esquina
	 */
	protected Set<Integer> vecinosEsquinas(int n) 
	{
		Set<Integer> ret= new HashSet<Integer>();
		
		if (n == 0) 
		{
			ret.add(1);
			ret.add(tamanio);
			return ret;
		}
		if (n == tamanio -1)
		{
			ret.add(n -1);
			ret.add(n +tamanio);
			return ret;
		}
		if (n == (vecinos.size()-tamanio))
		{
			ret.add(n +1);
			ret.add(n -tamanio);
			return ret;
		}
		ret.add(n -1);
		ret.add(n -tamanio);
			return ret;
		
	}
	
	/**
	 * Determina cuáles son los vecinos del entero de la primer fila n, 
	 * para devolver un Set con sus vecinos.
	 * @param n entero
	 * @return Retorna Set con los vecinos de n
	 */
	protected Set<Integer> vecinosPrimeraFila(int n) 
	{
		Set<Integer> ret= new HashSet<Integer>();
		ret.add(n -1);
		ret.add(n +1);
		ret.add(n +tamanio);
		return ret;
	}
	
	/**
	 * Determina cuáles son los vecinos del entero de la ultima fila n, 
	 * para devolver un Set con sus vecinos. 
	 * @param n entero
	 * @return Retorna Set con los vecinos de n
	 */
	protected Set<Integer> vecinosUltimaFila(int n) 
	{
		Set<Integer> ret= new HashSet<Integer>();
		ret.add(n -tamanio);	
		ret.add(n -1);
		ret.add(n +1);
		return ret;
	}
	
	/**
	 * Determina cuáles son los vecinos del entero de la primer columna n, 
	 * para devolver un Set con sus vecinos. 
	 * @param n entero
	 * @return Retorna Set con los vecinos de n
	 */
	protected Set<Integer> vecinosPrimeraColumna(int n) 
	{
		Set<Integer> ret= new HashSet<Integer>();
		ret.add(n -tamanio);
		ret.add(n +1);
		ret.add(n +tamanio);
		return ret;
	}
	
	/**
	 * Determina cuáles son los vecinos del entero de la ultima columna n, 
	 * para devolver un Set con sus vecinos. 
	 * @param n entero
	 * @return Retorna Set con los vecinos de n
	 */
	protected Set<Integer> vecinosUltimaColumna(int n) 
	{
		Set<Integer> ret= new HashSet<Integer>();
		ret.add(n -tamanio);
		ret.add(n -1);
		ret.add(n +tamanio);
		return ret;
	}
	
	/**
	 * Determina cuáles son los vecinos del entero del medio del vecindario n, 
	 * para devolver un Set con sus vecinos. 
	 * @param n entero
	 * @return Retorna Set con los vecinos de n
	 */	
	private Set<Integer> vecinosMedio(int n) 
	{
		Set<Integer> ret= new HashSet<Integer>();
		ret.add(n -tamanio);
		ret.add(n -1);
		ret.add(n +1);
		ret.add(n +tamanio);
		return ret;
	}
	
}
