package light_out_negocio;

import java.util.HashSet;
import java.util.Set;
import light_out_negocio.Vecindario;

public class VecindarioToroide extends Vecindario 
{
	/**
	 * Construye un vecindario de estructura toroide (usando un ArrayList de Sets de enteros). Simulando una matriz de tamaño n, cada Set se incicializa
	 * con un conjunto de los valores que tiene a cada lado, arriba y abajo.
	 * @param n tamaño del vecindario
	 */
	public VecindarioToroide(int n) 
	{
		super(n);
	}
	
	/**
	 * Crea los Sets del vecindario toroide y los inicializa con sus valores correspondientes.
	 */
	@Override
	protected void llenarVecinos()
	{
		for(int i = 0; i < tamanio*tamanio; i++)
			vecinos.add(new HashSet<Integer>());
		for(int i = 0; i < vecinos.size(); i++)
			vecinos.get(i).addAll(conjuntoDeVecinos(i));
	}
	
	/**
	 * Comprueba que esquina representa el entero n para devolver un Set con sus vecinos.
	 * @param n entero
	 * @return Retorna Set con los vecinos de la esquina
	 */
	@Override
	protected Set<Integer> vecinosEsquinas(int n) 
	{
		Set<Integer> ret= new HashSet<Integer>();
		
		if (n == 0) 
		{
			ret.add(1);
			ret.add(tamanio -1);
			ret.add(tamanio);
			ret.add(vecinos.size()-tamanio);
			return ret;
		}
		if (n == tamanio -1)
		{
			ret.add(0);
			ret.add(n -1);
			ret.add(n +tamanio);
			ret.add(vecinos.size() -1);
			return ret;
		}
		if (n == (vecinos.size()-tamanio))
		{
			ret.add(0);
			ret.add(n -tamanio);
			ret.add(n +1);
			ret.add(vecinos.size() -1);
			return ret;
		}
		ret.add(tamanio-1);
		ret.add(n -tamanio);
		ret.add(vecinos.size()-tamanio);
		ret.add(n -1);
			return ret;
	}
	
	/**
	 * Determina cuáles son los vecinos del entero de la primer fila n, 
	 * para devolver un Set con sus vecinos. 
	 * @param n entero
	 * @return Retorna Set con los vecinos de n
	 */
	@Override
	protected Set<Integer> vecinosPrimeraFila(int n) 
	{
		Set<Integer> ret= new HashSet<Integer>();
		ret.add(n -1);
		ret.add(n +1);
		ret.add(n +tamanio);
		ret.add(n +(tamanio*(tamanio-1)));
		return ret;
	}
	
	/**
	 * Determina cuáles son los vecinos del entero de la ultima fila n, 
	 * para devolver un Set con sus vecinos. 
	 * @param n entero
	 * @return Retorna Set con los vecinos de n
	 */
	@Override
	protected Set<Integer> vecinosUltimaFila(int n) 
	{
		Set<Integer> ret= new HashSet<Integer>();
		ret.add(n -(tamanio*(tamanio-1)));
		ret.add(n -1);
		ret.add(n +1);
		ret.add(n -tamanio);
		return ret;
	}
	
	/**
	 * Determina cuáles son los vecinos del entero de la primer columna n, 
	 * para devolver un Set con sus vecinos. 
	 * @param n entero
	 * @return Retorna Set con los vecinos de n
	 */	
	@Override
	protected Set<Integer> vecinosPrimeraColumna(int n) 
	{
		Set<Integer> ret= new HashSet<Integer>();	
		ret.add(n -tamanio);
		ret.add(n +1);
		ret.add(n +(tamanio -1));
		ret.add(n +tamanio);
		return ret;
	}
	
	/**
	 * Determina cuáles son los vecinos del entero de la ultima columna n, 
	 * para devolver un Set con sus vecinos. 
	 * @param n entero
	 * @return Retorna Set con los vecinos de n
	 */	
	@Override
	protected Set<Integer> vecinosUltimaColumna(int n) 
	{
		Set<Integer> ret= new HashSet<Integer>();
		ret.add(n -tamanio);
		ret.add(n -(tamanio -1)); 
		ret.add(n -1);
		ret.add(n +tamanio);
		return ret;
	}
}
