package light_out_negocio;

import static org.junit.Assert.*;
import java.util.Random;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import light_out_negocio.Tablero;

public class TableroTest 
{
	Tablero tablero3;
	Tablero tablero5;
	
	@Before
	public void inicializar()
	{
	tablero3 = new Tablero(3, false);
	tablero5 = new Tablero(5, true);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearTableroConParametroInsuficienteTest() 
	{
		@SuppressWarnings("unused")
		Tablero tablero= new Tablero(2,false);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearTableroConParametroExedidoTest() 
	{
		@SuppressWarnings("unused")
		Tablero tablero= new Tablero(6,false);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearTableroToroideConParametroInsuficienteTest() 
	{
		@SuppressWarnings("unused")
		Tablero tablero= new Tablero(2,true);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void crearTableroToroideConParametroExedidoTest() 
	{
		@SuppressWarnings("unused")
		Tablero tablero= new Tablero(6,true);
	}
	

	@Test(expected = IllegalArgumentException.class)
	public void cambiarLuzCasillaConParametroNegativoTest() 
	{
		tablero3.cambiarLuzCasilla(-1);
		tablero5.cambiarLuzCasilla(-1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void cambiarLuzCasillaConParametroExedidoTest() 
	{
		tablero3.cambiarLuzCasilla(9);
		tablero5.cambiarLuzCasilla(-1);
	}
	
	@Test
	public void cambiarLuzEsquinaSuperiorIzquierdaTest() 
	{
		boolean [] esperado = {tablero3.getValorCasilla(0),tablero3.getValorCasilla(1),tablero3.getValorCasilla(3)}; 	
		Set<Integer> casillasCambiadas = tablero3.cambiarLuzCasilla(0);
		assertTrue(tablero3.getValorCasilla(0)!= esperado[0] && tablero3.getValorCasilla(1)!=esperado[1] && tablero3.getValorCasilla(3)!=esperado[2]);
		assertTrue(casillasCambiadas.contains(1) && casillasCambiadas.contains(3) && casillasCambiadas.size()==2);
	}
	
	@Test
	public void cambiarLuzEsquinaSuperiorDerechaTest() 
	{
		boolean [] esperado = {tablero3.getValorCasilla(1),tablero3.getValorCasilla(2),tablero3.getValorCasilla(5)}; 	
		Set<Integer> casillasCambiadas = tablero3.cambiarLuzCasilla(2);
		assertTrue(tablero3.getValorCasilla(1)!= esperado[0] && tablero3.getValorCasilla(2)!=esperado[1] && tablero3.getValorCasilla(5)!=esperado[2]);
		assertTrue(casillasCambiadas.contains(1) && casillasCambiadas.contains(5) && casillasCambiadas.size()==2);
	}
	
	@Test
	public void cambiarLuzEsquinainferiorIzquierdaTest() 
	{
		boolean [] esperado = {tablero3.getValorCasilla(3),tablero3.getValorCasilla(6),tablero3.getValorCasilla(7)}; 	
		Set<Integer> casillasCambiadas = tablero3.cambiarLuzCasilla(6);
		assertTrue(tablero3.getValorCasilla(3)!= esperado[0] && tablero3.getValorCasilla(6)!=esperado[1] && tablero3.getValorCasilla(7)!=esperado[2]);
		assertTrue(casillasCambiadas.contains(3) && casillasCambiadas.contains(7) && casillasCambiadas.size()==2);
	}
	
	@Test
	public void cambiarLuzEsquinainferiorDerechaTest() 
	{
		boolean [] esperado = {tablero3.getValorCasilla(5),tablero3.getValorCasilla(7),tablero3.getValorCasilla(8)}; 	
		Set<Integer> casillasCambiadas = tablero3.cambiarLuzCasilla(8);
		assertTrue(tablero3.getValorCasilla(5)!= esperado[0] && tablero3.getValorCasilla(7)!=esperado[1] && tablero3.getValorCasilla(8)!=esperado[2]);
		assertTrue(casillasCambiadas.contains(5) && casillasCambiadas.contains(7) && casillasCambiadas.size()==2);
	}
	
	@Test
	public void cambiarLuzPrimerFilaTest() 
	{
		boolean [] esperado = {tablero3.getValorCasilla(0),tablero3.getValorCasilla(1),tablero3.getValorCasilla(2), tablero3.getValorCasilla(4)}; 	
		Set<Integer> casillasCambiadas = tablero3.cambiarLuzCasilla(1);
		assertTrue(tablero3.getValorCasilla(0)!= esperado[0] && tablero3.getValorCasilla(1)!=esperado[1] && tablero3.getValorCasilla(2)!=esperado[2] && tablero3.getValorCasilla(4)!=esperado[3]);
		assertTrue(casillasCambiadas.contains(0) && casillasCambiadas.contains(2) && casillasCambiadas.contains(4) && casillasCambiadas.size()==3);
	}
	
	@Test
	public void cambiarLuzUltimaFilaTest() 
	{
		boolean [] esperado = {tablero3.getValorCasilla(4),tablero3.getValorCasilla(6),tablero3.getValorCasilla(7), tablero3.getValorCasilla(8)}; 	
		Set<Integer> casillasCambiadas = tablero3.cambiarLuzCasilla(7);
		assertTrue(tablero3.getValorCasilla(4)!= esperado[0] && tablero3.getValorCasilla(6)!=esperado[1] && tablero3.getValorCasilla(7)!=esperado[2] && tablero3.getValorCasilla(8)!=esperado[3]);
		assertTrue(casillasCambiadas.contains(4) && casillasCambiadas.contains(6) && casillasCambiadas.contains(8) && casillasCambiadas.size()==3);
	}
	
	@Test
	public void cambiarLuzPrimerColumnaTest() 
	{
		boolean [] esperado = {tablero3.getValorCasilla(0),tablero3.getValorCasilla(3),tablero3.getValorCasilla(4), tablero3.getValorCasilla(6)}; 	
		Set<Integer> casillasCambiadas = tablero3.cambiarLuzCasilla(3);
		assertTrue(tablero3.getValorCasilla(0)!= esperado[0] && tablero3.getValorCasilla(3)!=esperado[1] && tablero3.getValorCasilla(4)!=esperado[2] && tablero3.getValorCasilla(6)!=esperado[3]);
		assertTrue(casillasCambiadas.contains(0) && casillasCambiadas.contains(4) && casillasCambiadas.contains(6) && casillasCambiadas.size()==3);
	}
	
	@Test
	public void cambiarLuzUltimaColumnaTest() 
	{
		boolean [] esperado = {tablero3.getValorCasilla(2),tablero3.getValorCasilla(4),tablero3.getValorCasilla(5), tablero3.getValorCasilla(8)}; 	
		Set<Integer> casillasCambiadas = tablero3.cambiarLuzCasilla(5);
		assertTrue(tablero3.getValorCasilla(2)!= esperado[0] && tablero3.getValorCasilla(4)!=esperado[1] && tablero3.getValorCasilla(5)!=esperado[2] && tablero3.getValorCasilla(8)!=esperado[3]);
		assertTrue(casillasCambiadas.contains(2) && casillasCambiadas.contains(4) && casillasCambiadas.contains(8) && casillasCambiadas.size()==3);
	}
	
	@Test
	public void cambiarLuzMedioTest() 
	{
		boolean [] esperado = {tablero3.getValorCasilla(1),tablero3.getValorCasilla(3),tablero3.getValorCasilla(4), tablero3.getValorCasilla(5), tablero3.getValorCasilla(7)}; 	
		Set<Integer> casillasCambiadas = tablero3.cambiarLuzCasilla(4);
		assertTrue(tablero3.getValorCasilla(1)!= esperado[0] && tablero3.getValorCasilla(3)!=esperado[1] && tablero3.getValorCasilla(4)!=esperado[2] && tablero3.getValorCasilla(5)!=esperado[3] && tablero3.getValorCasilla(7)!=esperado[4]);
		assertTrue(casillasCambiadas.contains(1) && casillasCambiadas.contains(3) && casillasCambiadas.contains(5) && casillasCambiadas.contains(7) && casillasCambiadas.size()==4);
	}
	
	@Test
	public void cambiarLuzToroideEsquinaSuperiorIzquierdaTest() 
	{
		boolean [] esperado = {tablero5.getValorCasilla(0),tablero5.getValorCasilla(1),tablero5.getValorCasilla(4), tablero5.getValorCasilla(5), tablero5.getValorCasilla(20)}; 	
		Set<Integer> casillasCambiadas = tablero5.cambiarLuzCasilla(0);
		assertTrue(tablero5.getValorCasilla(0)!= esperado[0] && tablero5.getValorCasilla(1)!=esperado[1] && tablero5.getValorCasilla(4)!=esperado[2] && tablero5.getValorCasilla(5)!=esperado[3] && tablero5.getValorCasilla(20)!=esperado[4]);
		assertTrue(casillasCambiadas.contains(1) && casillasCambiadas.contains(4) && casillasCambiadas.contains(5) && casillasCambiadas.contains(20) && casillasCambiadas.size()==4);
	}
	
	@Test
	public void cambiarLuzToroideEsquinaSuperiorDerechaTest() 
	{
		boolean [] esperado = {tablero5.getValorCasilla(0),tablero5.getValorCasilla(3),tablero5.getValorCasilla(4), tablero5.getValorCasilla(9), tablero5.getValorCasilla(24)}; 	
		Set<Integer> casillasCambiadas = tablero5.cambiarLuzCasilla(4);
		assertTrue(tablero5.getValorCasilla(0)!= esperado[0] && tablero5.getValorCasilla(3)!=esperado[1] && tablero5.getValorCasilla(4)!=esperado[2] && tablero5.getValorCasilla(9)!=esperado[3] && tablero5.getValorCasilla(24)!=esperado[4]);
		assertTrue(casillasCambiadas.contains(0) && casillasCambiadas.contains(3) && casillasCambiadas.contains(9) && casillasCambiadas.contains(24) && casillasCambiadas.size()==4);
	}
	
	@Test
	public void cambiarLuzToroideEsquinainferiorIzquierdaTest() 
	{
		boolean [] esperado = {tablero5.getValorCasilla(0),tablero5.getValorCasilla(15),tablero5.getValorCasilla(20), tablero5.getValorCasilla(21), tablero5.getValorCasilla(24)}; 	
		Set<Integer> casillasCambiadas = tablero5.cambiarLuzCasilla(20);
		assertTrue(tablero5.getValorCasilla(0)!= esperado[0] && tablero5.getValorCasilla(15)!=esperado[1] && tablero5.getValorCasilla(20)!=esperado[2] && tablero5.getValorCasilla(21)!=esperado[3] && tablero5.getValorCasilla(24)!=esperado[4]);
		assertTrue(casillasCambiadas.contains(0) && casillasCambiadas.contains(15) && casillasCambiadas.contains(21) && casillasCambiadas.contains(24) && casillasCambiadas.size()==4);
	}
	
	@Test
	public void cambiarLuzToroideEsquinainferiorDerechaTest() 
	{
		boolean [] esperado = {tablero5.getValorCasilla(4),tablero5.getValorCasilla(19),tablero5.getValorCasilla(20), tablero5.getValorCasilla(23), tablero5.getValorCasilla(24)}; 	
		Set<Integer> casillasCambiadas = tablero5.cambiarLuzCasilla(24);
		assertTrue(tablero5.getValorCasilla(4)!= esperado[0] && tablero5.getValorCasilla(19)!=esperado[1] && tablero5.getValorCasilla(20)!=esperado[2] && tablero5.getValorCasilla(23)!=esperado[3] && tablero5.getValorCasilla(24)!=esperado[4]);
		assertTrue(casillasCambiadas.contains(4) && casillasCambiadas.contains(19) && casillasCambiadas.contains(20) && casillasCambiadas.contains(23) && casillasCambiadas.size()==4);
	}
	
	@Test
	public void cambiarLuzToroidePrimerFilaTest() 
	{
		boolean [] esperado = {tablero5.getValorCasilla(1),tablero5.getValorCasilla(2),tablero5.getValorCasilla(3), tablero5.getValorCasilla(7), tablero5.getValorCasilla(22)}; 	
		Set<Integer> casillasCambiadas = tablero5.cambiarLuzCasilla(2);
		assertTrue(tablero5.getValorCasilla(1)!= esperado[0] && tablero5.getValorCasilla(2)!=esperado[1] && tablero5.getValorCasilla(3)!=esperado[2] && tablero5.getValorCasilla(7)!=esperado[3] && tablero5.getValorCasilla(22)!=esperado[4]);
		assertTrue(casillasCambiadas.contains(1) && casillasCambiadas.contains(3) && casillasCambiadas.contains(7) && casillasCambiadas.contains(22) && casillasCambiadas.size()==4);
	}
	
	@Test
	public void cambiarLuzToroideUltimaFilaTest() 
	{
		boolean [] esperado = {tablero5.getValorCasilla(2),tablero5.getValorCasilla(17),tablero5.getValorCasilla(21), tablero5.getValorCasilla(22), tablero5.getValorCasilla(23)}; 	
		Set<Integer> casillasCambiadas = tablero5.cambiarLuzCasilla(22);
		assertTrue(tablero5.getValorCasilla(2)!= esperado[0] && tablero5.getValorCasilla(17)!=esperado[1] && tablero5.getValorCasilla(21)!=esperado[2] && tablero5.getValorCasilla(22)!=esperado[3] && tablero5.getValorCasilla(23)!=esperado[4]);
		assertTrue(casillasCambiadas.contains(2) && casillasCambiadas.contains(17) && casillasCambiadas.contains(21) && casillasCambiadas.contains(23) && casillasCambiadas.size()==4);
	}
	
	@Test
	public void cambiarLuzToroidePrimerColumnaTest() 
	{
		boolean [] esperado = {tablero5.getValorCasilla(5),tablero5.getValorCasilla(10),tablero5.getValorCasilla(11), tablero5.getValorCasilla(14), tablero5.getValorCasilla(15)}; 	
		Set<Integer> casillasCambiadas = tablero5.cambiarLuzCasilla(10);
		assertTrue(tablero5.getValorCasilla(5)!= esperado[0] && tablero5.getValorCasilla(10)!=esperado[1] && tablero5.getValorCasilla(11)!=esperado[2] && tablero5.getValorCasilla(14)!=esperado[3] && tablero5.getValorCasilla(15)!=esperado[4]);
		assertTrue(casillasCambiadas.contains(5) && casillasCambiadas.contains(11) && casillasCambiadas.contains(14) && casillasCambiadas.contains(15) && casillasCambiadas.size()==4);
	}
	
	@Test
	public void cambiarLuzToroideUltimaColumnaTest() 
	{
		boolean [] esperado = {tablero5.getValorCasilla(9),tablero5.getValorCasilla(10),tablero5.getValorCasilla(13), tablero5.getValorCasilla(14), tablero5.getValorCasilla(19)}; 	
		Set<Integer> casillasCambiadas = tablero5.cambiarLuzCasilla(14);
		assertTrue(tablero5.getValorCasilla(9)!= esperado[0] && tablero5.getValorCasilla(10)!=esperado[1] && tablero5.getValorCasilla(13)!=esperado[2] && tablero5.getValorCasilla(14)!=esperado[3] && tablero5.getValorCasilla(19)!=esperado[4]);
		assertTrue(casillasCambiadas.contains(9) && casillasCambiadas.contains(10) && casillasCambiadas.contains(13) && casillasCambiadas.contains(19) && casillasCambiadas.size()==4);
	}
	
	@Test
	public void tablero3ResueltoArregloTest() 
	{
		Tablero tablero= ejemploDeTableroResuelto(3);
		boolean[] b= new boolean[9];
		
		for (int i=0; i<b.length; i++)
			b[i]= tablero.getValorCasilla(i);
		
		assertArrayEquals(new boolean[9],b);
	}
	
	@Test
	public void tablero3ResueltoContadorTest() 
	{
		Tablero tablero= ejemploDeTableroResuelto(3);		
		assertTrue(tablero.getContador()==9);
	}
	
	@Test
	public void tablero3NoResueltoContadorTest() 
	{		
		assertFalse(tablero3.getContador()==9);
		assertTrue(tablero3.getContador()>=0 && tablero3.getContador()<9);
	}
	
	@Test
	public void tablero5NoResueltoContadorTest() 
	{
		assertFalse(tablero5.getContador()==25);
		assertTrue(tablero5.getContador()>=0 && tablero5.getContador()<25);
	}
	
	private Tablero ejemploDeTableroResuelto(int n)
	{
		Random r= new Random();
		Tablero tablero = new Tablero (n, false);
		
		while (!tablero.tableroResuelto())
			tablero.cambiarLuzCasilla(r.nextInt(n*n));
	
		return tablero;
	}	
}