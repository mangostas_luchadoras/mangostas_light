package light_out_negocio;

public class Casilla 
{
	private boolean luz;
	
	/**
	 * Construye una casilla representada con un boolean.
	 * @param l valor de la luz (false apagada, true prendida)
	 */
	Casilla(boolean l)
	{
		luz = l;
	}
	
	/**
	 * Cambia el valor de luz de la casilla.
	 * @return entero 1 si se apago una luz, entero -1 si se prendió
	 */
	public int cambiarLuz()
	{
		luz = !luz;	
		return luz==false? 1 : -1;
	}	
	
	public boolean getLuz() 
	{
		return luz;
	}
	
}
