package light_out_negocio;

import java.util.Random;
import java.util.Set;
import light_out_negocio.Casilla;
import light_out_negocio.Vecindario;
import light_out_negocio.VecindarioToroide;

public class Tablero 
{
	private Casilla[] tablero;
	private Vecindario vecinos;
	private int contadorLucesApagadas;
	private boolean modoToroide;
	
	/**
	 * Construye un tablero de casillas con valores booleans random.
	 * @param n tamaño del tablero
	 * @param b modo del tablero (false simple, true modo toroide)
	 */
	public Tablero(int n, boolean b)
	{
		verificarParametroConstructor(n);
		tablero = new Casilla[n*n];
		llenarTablero();
		modoToroide = b;
		if(modoToroide==false) 
			vecinos = new Vecindario(n);
		else 
			vecinos = (Vecindario) new VecindarioToroide(n);
	}	
	
	/**
	 * Inicializa un tablero con casillas de valores boolean random, asegurando que nuca sean todos los valores false.
	 */
	private void llenarTablero()
	{
		Random r = new Random();
		do
		{
			for(int i = 0; i < tablero.length; i++) 
			{
				tablero[i] = new Casilla(r.nextBoolean());
				if (tablero[i].getLuz() == false)
					contadorLucesApagadas++;
			}
		} while(tableroResuelto());
	}	
	
	/**
	 * Compara el tamaño del tablero con la cantidad de luces apagadas del mismo.
	 * @return Retorna boolean (true or false)
	 */
	public boolean tableroResuelto()
	{
		return tablero.length == contadorLucesApagadas;
	}
	
	/**
	 * Cambia el valor (true or false) de la casilla n, así como la de sus casillas vecinas.
	 * @param n Ubicación de la casilla a cambiar
	 * @return Rotorna Set con el indice de las casillas vecinas
	 */
	public Set<Integer> cambiarLuzCasilla(int n) 
	{
		verificarParametroCasilla(n);	
		contadorLucesApagadas+= tablero[n].cambiarLuz();
		for(Integer i: vecinos.getVecinos(n))
			contadorLucesApagadas+= tablero[i].cambiarLuz();
		return vecinos.getVecinos(n);

	}

	/**
	 * Verifica que el entero n se encuentre en un rango correcto.
	 * @param n entero
	 * @throws IllegalArgumentException si el entero n no se encuentra en el rango (3-5)
	 */
	private void verificarParametroConstructor(int n)
	{
		if (n<3 || n>5)
			throw new IllegalArgumentException("El valor " + n + " no se encuentra en el rango valido: 3-5");
	}
	
	/**
	 * Verifica que el entero n se encuentre en un rango correcto.
	 * @param n entero
	 * @throws IllegalArgumentException si el entero n no se encuentra en el rango (0-(tamaño del tablero -1))
	 */
	private void verificarParametroCasilla(int n)
	{
		if (n < 0 || n >= tablero.length)
			throw new IllegalArgumentException("El valor " + n + " no se encuentra en el rango valido: 0-" + (tablero.length-1));
	}
	
	public boolean getValorCasilla(int n) 
	{
		return tablero[n].getLuz();
	}
	
	public int getContador() 
	{
		return contadorLucesApagadas;
	}
	
	public void setModoToroide()
	{
		modoToroide = !modoToroide;
	}
	
	public boolean getModoToroide()
	{
		return modoToroide;
	}
	
}