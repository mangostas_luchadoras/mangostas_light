package light_out_gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.Timer;
import light_out_negocio.Tablero;
import java.awt.Toolkit;

public class Juego 
{
	private JFrame frmMangostasLightOut;
	private Fondo presentacion;
	private int tamanio;
	private Tablero tablero;
	private Menu menu;
	private TableroGui tableroGui;
	private Controles controles;
	private Terminado ganaste;
	private Fondo animacion;
	private BarraMenu barraMenu;
		
	/**
	 * Activa la aplicación.
	 */
	public static void main(String[] args) 
	{
		
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					Juego ventana = new Juego();
					ventana.frmMangostasLightOut.setVisible(true);
					Timer timer = new Timer(5000, new ActionListener()
					{
					    @Override  
						public void actionPerformed(ActionEvent evt)
					    {
					    	ventana.presentacion.setVisible(false);
					    	ventana.menu.setVisible(true);
					    }
					});
					timer.setRepeats(false);		
					timer.start();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
		Sonido.reproducirLoop("tiempo_loop.wav");
		
	}

	/**
	 * Crea el juego.
	 */
	public Juego() 
	{
		initialize();
	}
	
	/**
	 * Inicializa el contenido del frame.
	 */
	private void initialize() 
	{
		frmMangostasLightOut = new JFrame();
		
		tamanio = 3;
		tablero = new Tablero(tamanio, false);
		tableroGui = new TableroGui(this);
		controles = new Controles(this);
		menu = new Menu(this, "/image/mangosta_menu.png");
		presentacion = new Fondo("/image/mangosta_presentacion.png");
		ganaste = new Terminado(this, "/image/mangosta_terminado.png");
		animacion = new Fondo("/image/mangostarock.gif");
		barraMenu = new BarraMenu();
		
		setearAnimacion();		
		setearVentana();
		agregarComponentes();
		ocultarComponentes();

		frmMangostasLightOut.getContentPane().repaint();			
	}

	/**
	 * Setea los margenes y fondo del gif animado.
	 */
	private void setearAnimacion() 
	{
		animacion.setBoundsFondo(0, 0, 200, 179);
		animacion.setBounds(370, 190, 200, 179);
		animacion.setBackground(new Color(39,22,86));
	}

	/**
	 * Setea las propiedades del la ventana principal.
	 */
	private void setearVentana() 
	{
		frmMangostasLightOut.setBounds(100, 100, 600, 430);
		frmMangostasLightOut.setResizable(false);
		frmMangostasLightOut.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMangostasLightOut.getContentPane().setLayout(null);
		frmMangostasLightOut.setIconImage(Toolkit.getDefaultToolkit().getImage(Juego.class.getResource("/image/mangosta_icon.jpeg")));
		frmMangostasLightOut.setTitle("Mangosta Light Out");
		frmMangostasLightOut.getContentPane().setBackground(new Color(39,22,86));
	}
	
	/**
	 * Agrega todos los componentes que se visualizan en la ventana.
	 */
	private void agregarComponentes() 
	{
		frmMangostasLightOut.getContentPane().add(presentacion);
		frmMangostasLightOut.getContentPane().add(menu);
		frmMangostasLightOut.getContentPane().add(tableroGui);
		frmMangostasLightOut.getContentPane().add(controles);
		frmMangostasLightOut.getContentPane().add(ganaste);
		frmMangostasLightOut.getContentPane().add(animacion);
		frmMangostasLightOut.setJMenuBar(barraMenu);
	}
	
	/**
	 * Oculta aquellos componentes que no deben verse al principio del juego.
	 */
	private void ocultarComponentes() 
	{
		barraMenu.setVisible(false);				
		menu.setVisible(false);
		tableroGui.setVisible(false);
		controles.setVisible(false);
		animacion.setVisible(false);
		ganaste.setVisible(false);
	}
	
	/**
	 * Actualiza el tablero gui a partir de un nuevo tablero con tamaño n y reproduce un sonido click.
	 * @param n entero
	 */
	void actualizarTablero(int n) 
	{
		tamanio = n;
		resetTableroGui();
	}
	
	/**
	 * Resetea el tablero gui a partir de un nuevo tablero.
	 */
	void resetTableroGui() 
	{
		tablero = new Tablero(tamanio,tablero.getModoToroide());
		frmMangostasLightOut.getContentPane().remove(tableroGui);
		tableroGui = new TableroGui(this);
		frmMangostasLightOut.getContentPane().add(tableroGui);
		frmMangostasLightOut.getContentPane().revalidate();
		frmMangostasLightOut.getContentPane().repaint();		
	}
	
	/**
	 * Hace visible la pantalla de juego Terminado y reproduce el sonido ganador.
	 */
	public void ganaste() 
	{
		controles.setVisible(false);
		tableroGui.setVisible(false);
		animacion.setVisible(false);
		barraMenu.setVisible(false);
		ganaste.setVisible(true);
		Sonido.reproducir("ganador.wav",1000);
		frmMangostasLightOut.getContentPane().repaint();
	}
	
	/**
	 * Activa/desactiva el modo toroide.
	 */
	void setModoToroide() 
	{
		tablero.setModoToroide();
	}
	
	/**
	 * Devuelve un boolean que representa si esta activo el modo toroide.
	 * @return estado del modo toroide.
	 */
	boolean getModoToroide() 
	{
		return tablero.getModoToroide();
	}
	
	/**
	 * Devuelve el tamanio del tablero.
	 * @return tamanio del tablero.
	 */
	public int getTamanio() 
	{
		return tamanio;
	}

	/**
	 * Devuelve el Tablero del juego.
	 * @return Tablero de juego.
	 */
	public Tablero getTablero() 
	{
		return tablero;
	}
	
	/**
	 * Devuelve el Menu del juego.
	 * @return Menu del juego.
	 */
	public Menu getMenu() 
	{
		return menu;
	}
	
	/**
	 * Devuelve los Controles del juego.
	 * @return Controles del juego.
	 */
	public Controles getControles() 
	{
		return controles;
	}
	
	/**
	 * Devuelve la animacion de la ventana de juego.
	 * @return la animacion de la ventana de juego.
	 */
	public Fondo getAnimacion()
	{
		return animacion;
	}
	
	/**
	 * Devuelve la barra de menu.
	 * @return la barra de menu.
	 */
	public BarraMenu getBarraMenu()
	{
		return barraMenu;
	}
}
