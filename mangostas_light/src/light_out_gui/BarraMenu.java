package light_out_gui;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class BarraMenu extends JMenuBar 
{
	private static final long serialVersionUID = 1L;
	private JMenu archivo;
	private JMenu ayuda;

	/**
	 * Crea una barra de menú con los distintos menús e items que esta contiene.
	 */
	public BarraMenu() 
	{
		archivo = new JMenu("Archivo");
		ayuda = new JMenu("Ayuda");
		llenarMenuArchivo();
		llenarMenuAyuda();
		add(archivo);
		add(ayuda);
	}

	/**
	 * Crea y setea los Items del menú Ayuda y los agrega a este.
	 */
	private void llenarMenuAyuda() 
	{
		JMenuItem mntmInstrucciones = new JMenuItem("Instrucciones");
		mntmInstrucciones.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				JDialog instruciones = new JDialog();
				instruciones.setBounds(150, 150, 450, 330);
				instruciones.setResizable(false);
				instruciones.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				instruciones.getContentPane().setLayout(null);
				instruciones.setIconImage(Toolkit.getDefaultToolkit().getImage(Juego.class.getResource("/image/mangosta_icon.jpeg")));
				instruciones.setTitle("Mangosta Light Out Instrucciones");
				Fondo image = new Fondo("/image/mangosta_instrucciones.png");
				image.setBoundsFondo(0, 0, 450, 300);
				instruciones.getContentPane().add(image);
				instruciones.setVisible(true);
			}
		});
		ayuda.add(mntmInstrucciones);
		
		JMenuItem mntmAcercaDe = new JMenuItem("Acerca de ...");
		mntmAcercaDe.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				JDialog acerca = new JDialog();
				acerca.setBounds(150, 150, 450, 330);
				acerca.setResizable(false);
				acerca.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				acerca.getContentPane().setLayout(null);
				acerca.setIconImage(Toolkit.getDefaultToolkit().getImage(Juego.class.getResource("/image/mangosta_icon.jpeg")));
				acerca.setTitle("Acerca de Mangosta Light Out");
				Fondo image = new Fondo("/image/mangosta_acerca_de.png");
				image.setBoundsFondo(0, 0, 450, 300);
				acerca.getContentPane().add(image);
				acerca.setVisible(true);
			}
		});
		ayuda.add(mntmAcercaDe);
	}

	/**
	 * Crea y setea los Items del menú Archivo y los agrega a este.
	 */
	private void llenarMenuArchivo() 
	{
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mntmSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				System.exit(0);
			}
		});
		archivo.add(mntmSalir);
	}
}
