package light_out_gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.LineBorder;

public class TableroGui extends JPanel 
{
	private static final long serialVersionUID = 1L;
	private JCheckBox[] casillas;
	private boolean resuelto;
	
	/**
	 * Crea un tablero de casillas representados con imágenes en JChekbox's.
	 * Cada botón representa la casilla de un tablero y cambia de valor al hacer click, dependiendo del modo de juego.
	 * @param juego donde se va a representar el tablero gui
	 */
	public TableroGui(Juego juego) 
	{
		setBorder(new LineBorder(new Color(0, 0, 0), 5));
		setBounds(20, 20, 22+(64*juego.getTamanio()), 22+(64*juego.getTamanio()));
		setBackground(Color.BLACK);
		setLayout(new GridLayout(juego.getTamanio(), juego.getTamanio(), 5, 5));
		resuelto = juego.getTablero().tableroResuelto();
		
		casillas = new JCheckBox[juego.getTamanio()*juego.getTamanio()];
				
		for (int i = 0; i < casillas.length; i++) 
		{
			int indice=i;
			casillas[i] = new JCheckBox();
			casillas[i].setSelectedIcon(new ImageIcon(Juego.class.getResource("/image/serpiente_prendida.png")));
			casillas[i].setIcon(new ImageIcon(Juego.class.getResource("/image/mangosta_apagada.png")));
			casillas[i].setHorizontalAlignment(SwingConstants.CENTER);
			casillas[i].setSelected(juego.getTablero().getValorCasilla(i));
				
			casillas[i].addMouseListener(new MouseAdapter() 
			{
				@Override
				public void mouseClicked(MouseEvent e) 
				{
					Sonido.reproducir("click.wav", 50);
					Set<Integer> casillasParaCambiar = juego.getTablero().cambiarLuzCasilla(indice);
					for (Integer j:casillasParaCambiar)
						cambiarValorCasilla(j);
					resuelto = juego.getTablero().tableroResuelto();
					if (resuelto) 
					{
						Timer timer = new Timer(600, new ActionListener()
						{
						    @Override  
							public void actionPerformed(ActionEvent evt)
						    {
						    	juego.ganaste();
						    }
						});
						timer.setRepeats(false);		
						timer.start();
					}
						
					}
			});
			add(casillas[i]);
		}
	}

	/**
	 * Cambia el valor de una casilla en el tablero gui
	 * @param n entero
	 */
	private void cambiarValorCasilla(int n)
	{
		if(casillas[n].isSelected())
			casillas[n].setSelected(false);
		else
			casillas[n].setSelected(true);
	}
	
}
