package light_out_gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Terminado extends Fondo 
{
	private static final long serialVersionUID = 1L;
	private JButton btnVolver;

	/**
	 * Crea un panel que representa el fin del juego, con un botón para volver al menú.
	 * @param juego donde se va a representar el terminado
	 * @param s ruta de la imagen
	 */
	public Terminado(Juego juego, String s) 
	{
		super(s);
		super.borrarFondo();
		
		btnVolver = new JButton();
		btnVolver.setBorder(null);
		btnVolver.setBorderPainted(false);
		btnVolver.setContentAreaFilled(false);
		btnVolver.setIcon(new ImageIcon(Menu.class.getResource("/image/mangostita.png")));
		btnVolver.setBounds(220, 140, 150, 200);
		btnVolver.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				Sonido.reproducir("click.wav",50);
				setVisible(false);
				juego.getMenu().setVisible(true);
				
			}
		});
		add(btnVolver);
		
		super.pintarFondo();
	}

}
