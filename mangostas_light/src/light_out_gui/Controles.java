package light_out_gui;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;

public class Controles extends JPanel 
{
	private static final long serialVersionUID = 1L;
	private static Font fuente = new Font("Comic Sans MS", 16, 16);
	private JButton btn3x3;
	private JButton btn4x4;
	private JButton btn5x5;
	private JButton btnReset;
	private JButton btnModoToroide;

	/**
	 * Crea los botones del juego.
	 * TABLERO 3x3: Cambia a un nuevo tablero de 3x3.
	 * TABLERO 4x4: Cambia a un nuevo tablero de 4x4.
	 * TABLERO 5x5: Cambia a un nuevo tablero de 5x5.
	 * RESET: Resetea el tablero actual.
	 * TOROIDE: Activa/desactiva el modo toroide.
	 * @param juego donde se van a representar los controles
	 */
	public Controles(Juego juego)
	{

		setLayout(new GridLayout(0, 1, 0, 0));
		setBounds(400, 20, 150, 150);
		
		btn3x3 = new JButton("TABLERO 3x3");
		btn3x3.setFont(fuente);
		btn3x3.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				Sonido.reproducir("click.wav",50);
				juego.actualizarTablero(3);
			}
		});
		add(btn3x3);
		
		btn4x4 = new JButton("TABLERO 4x4");
		btn4x4.setFont(fuente);
		btn4x4.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				Sonido.reproducir("click.wav",50);
				juego.actualizarTablero(4);

			}
		});
		add(btn4x4);
		
		btn5x5 = new JButton("TABLERO 5x5");
		btn5x5.setFont(fuente);
		btn5x5.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				Sonido.reproducir("click.wav",50);
				juego.actualizarTablero(5);
			}
		});
		add(btn5x5);
		
		btnReset = new JButton("RESET");
		btnReset.setFont(fuente);
		btnReset.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				Sonido.reproducir("click.wav",50);
				juego.resetTableroGui();
			}
		});
		add(btnReset);
		
		btnModoToroide = new JButton("TOROIDE");
		btnModoToroide.setFont(fuente);
		btnModoToroide.setForeground(Color.WHITE);
		btnModoToroide.setBackground(Color.red);
		btnModoToroide.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				Sonido.reproducir("click.wav",50);
				juego.setModoToroide();
				juego.resetTableroGui();
				if (juego.getModoToroide()==true)
					btnModoToroide.setBackground(Color.green);

				else
					btnModoToroide.setBackground(Color.red);
			}
		});
		add(btnModoToroide);	
	}
}

