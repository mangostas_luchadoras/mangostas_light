package light_out_gui;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Fondo extends JPanel 
{
	private static final long serialVersionUID = 1L;
	private JLabel fondo;
	
	/**
	 * Carga y dibuja una imagen de fondo.
	 * @param s ruta de la imagen.
	 */
	public Fondo(String s)
	{
		setBounds(0, 0, 600, 400);
		setLayout(null);
		fondo = new JLabel();
		fondo.setBounds(0, 0, 600, 400);
		ImageIcon imagen = new ImageIcon(this.getClass().getResource(s));
		fondo.setIcon(imagen);
		add(fondo);
	}

	/**
	 * Quita la imagen del Fondo.
	 */
	protected void borrarFondo() 
	{
		remove(fondo);
	}
	
	/**
	 * Dibuja la imagen del Fondo.
	 */
	protected void pintarFondo() 
	{
		add(fondo);
	}
	
	/**
	 * Setea las medidas del margen de la imagen.
	 * @param x coordenada x del margende la imagen.
	 * @param y coordenada y del margende la imagen.
	 * @param ancho ancho total del margende la imagen.
	 * @param alto alto total del margen de la imagen.
	 */
	public void setBoundsFondo(int x, int y, int ancho, int alto)
	{
		fondo.setBounds(x, y, ancho, alto);
	}
}
