package light_out_gui;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Menu extends Fondo 
{
	private static final long serialVersionUID = 1L;
	private JButton btnStart;
	
	/**
	 * Crea un menú previo al juego con un botón para empezar.
	 * @param juego donde se va a representar el menú
	 * @param s ruta de la imagen
	 */
	public Menu(Juego juego, String s) 
	{
		super(s);
		super.borrarFondo();
		
		btnStart = new JButton();
		btnStart.setBorder(null);
		btnStart.setBorderPainted(false);
		btnStart.setContentAreaFilled(false);
		btnStart.setIcon(new ImageIcon(Menu.class.getResource("/image/mangosta_farol_on.png")));
		btnStart.setPressedIcon(new ImageIcon(this.getClass().getResource("/image/mangosta_farol_off.png")));
		btnStart.setBounds(74, 152, 109, 139);
		btnStart.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				setVisible(false);
				Sonido.reproducir("fuego.wav",500);
				juego.resetTableroGui();
				juego.getControles().setVisible(true);
				juego.getAnimacion().setVisible(true);
				juego.getBarraMenu().setVisible(true);
			}
		});
		add(btnStart);
		
		super.pintarFondo();
	}
}
