package light_out_gui;

import java.applet.AudioClip;
import java.io.File;
import java.net.URL;
import javax.swing.JApplet;

public class Sonido 
{
	/**
	 * Reproduce un archivo de audio wav.
	 * @param s ruta del archivo de audio
	 * @param n entero (milisegundos de reproducción)
	 */
	public static void reproducir(String s, int n) 
	 { 
		 try 
		 {
			 File f=new File(s);
			 URL u = f.toURI().toURL();
			 AudioClip sonido=JApplet.newAudioClip(u);
			 sonido.play();
			 Thread.currentThread();
			 Thread.sleep(n);
			 sonido.stop();
		 }
		 catch (Exception ex) 
		 {
		  System.out.println (ex);
		 }
    }
	
	/**
	 * Reproduce un archivo de audio wav en forma constante.
	 * @param s ruta del archivo de audio
	 */
	public static void reproducirLoop(String s) 
	{ 
		try 
		{
			File f=new File(s);
			URL u = f.toURI().toURL();
			AudioClip sonido=JApplet.newAudioClip(u);
			sonido.loop();
			Thread.sleep(1000000000);
		}
		catch (Exception ex) 
		{
			System.out.println (ex);
		}
    }

}